import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import reducer from './reducers'
import Routes from './routes.js'
import './assets/scss/base.scss'

const store = createStore(reducer);


class App extends Component {
    render() {

        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Routes/>
                </BrowserRouter>
            </Provider>

        );
    }
}

ReactDOM.render((<App/>), document.getElementById('app'));