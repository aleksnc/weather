import React from 'react';
import { Switch, Route } from 'react-router-dom'

import Home from './pages/Home/Home.js';
import City from './pages/City/City.js';

const Routes = () => (
    <Switch>
        <Route exact  path='/' component={Home}/>
        <Route  path='/:number?' component={City}/>
    </Switch>
)

export default Routes;

