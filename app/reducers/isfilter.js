import {IS_FILTER} from "../constants";

const initialState = false;

function reducers(state = initialState, action) {
    if (action.type === IS_FILTER) {
        return action.content
    }
    return state;
}

export default reducers
