import {ALL_ID} from "../constants";
import DATA from '../utilits/city.list.min.json'

let like=[];
let storageData = JSON.parse(localStorage.getItem("like"));

if (storageData === null) {
    localStorage.setItem("like", JSON.stringify(like));
} else {
    like = storageData
}

let dataArr =DATA.map((item, index) => {
    return item.id;
});

const initialState = like .filter(x => !dataArr.includes(x)||dataArr.includes(x))
    .concat(dataArr.filter(x => !like.includes(x)));

function reducers(state = initialState, action) {
    if (action.type === ALL_ID) {
        return action.content
    }
    return state;
}

export default reducers
