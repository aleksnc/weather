import {SORT_AZ} from "../constants";

const initialState = false;

function reducers(state = initialState, action) {
    if (action.type === SORT_AZ) {
        return action.content
    }
    return state;
}

export default reducers
