import {LIST__WEATHER} from "../constants";

const initialState = null;

function reducers(state = initialState, action) {
    if (action.type === LIST__WEATHER) {
        return action.content
    }
    return state;
}

export default reducers
