import {LIKE_ID} from "../constants";

let like=[];
let storageData = JSON.parse(localStorage.getItem("like"));

if (storageData === null) {
    localStorage.setItem("like", JSON.stringify(like));
} else {
    like = storageData
}
const initialState = like;


function reducers(state = initialState, action) {
    if (action.type === LIKE_ID) {
        return action.content
    }
    return state;
}

export default reducers
