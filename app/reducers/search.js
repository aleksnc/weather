import {SEARCH_CITY} from "../constants";

const initialState = '';

function reducers(state = initialState, action) {
    if (action.type === SEARCH_CITY) {
        return action.content
    }
    return state;
}

export default reducers
