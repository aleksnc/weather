import {combineReducers} from 'redux'
import allCityID from './allCityID.js';
import likeCityID from './likeCityID.js';
import search from './search.js';
import sortAZ from './sortAZ.js';
import isfilter from './isfilter.js';

export default combineReducers({
    allCityID,
    likeCityID,
    search,
    sortAZ,
    isfilter
})
