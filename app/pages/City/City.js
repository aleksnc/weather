import React, {Component} from 'react';
import {connect} from 'react-redux'

import './city.scss'

import AboutWeather from '../../components/AboutWeather/AboutWeather'

class City extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            posts: [],
            listWeather: [],
        }

        this.getSubreddit = this.getSubreddit.bind(this);
    }


    getSubreddit(id) {
        let main = this;
        //fetch('http://api.openweathermap.org/data/2.5/weather?id='+id+'&units=metric&appid=69465df65a432b95653f0f7d02072c98')
        fetch('http://api.openweathermap.org/data/2.5/weather?id=' + id + '&units=metric&appid=120509f47800264392c68f94ff38fb25')
            .then(response => {
                return response.json();
            }).then(json => {
            const listWeather = json;
            main.state.listWeather.push(listWeather);
            main.setState({posts: listWeather})
        }).catch(err => {
            console.log('Cannot find subreddit.');
        });
    }

    componentWillMount() {
        let num = this.props.match.params.number || null;


        if (num === null) {
            window.location = "/"
        }

        this.setState({
            num: num,
        })

    }

    componentDidMount() {
        this.getSubreddit(this.state.num)
    }

    render() {
        return (
            <div className="sity">
                {
                    this.state.listWeather.map((item, index) => (
                        <AboutWeather key={index} content={item}/>
                    ))
                }
            </div>
        )

    }
}

export default connect(
    state => ({
    }),
    dispatch => ({})
)(City);