import React, { Component } from "react";
import { connect } from "react-redux";

import "./home.scss";
import MySvg from "../../assets/mySVG.js";

import CartCity from "../../components/CartCity/CartCity";
import NavButton from "../../components/NavButton/NavButton";
import Search from "../../components/Search/Search";

import { IS_FILTER } from "../../constants";

class Home extends Component {
    constructor( props ) {
        super( props );

        this.state = {
            page: 1,
            posts: '',
            statNext: true,
            statPrev: false,
            listWeather: [],
            load: false
        }

        this.getSubreddit = this.getSubreddit.bind( this );
        this.getEach = this.getEach.bind( this );
        this.onClickPagging = this.onClickPagging.bind( this );
    }

    onClickPagging( e ) {
        if(e.target.id===''){
            return;
        }

        this.setState( { load: false } );

        let pages = this.getPages();

        let page = this.state.page;
        let statNext = true;
        let statPrev = true;

        if ( e.target.id === 'next' ) {
            page++;
        } else {
            page--;
        }

        if ( pages % 1 === 0 && page === pages || page > pages ) {
            statNext = false;
        }

        if ( page === 1 ) {
            statPrev = false;
        }

        this.setState( {
            page: page,
            statPrev: statPrev,
            statNext: statNext
        } );

        this.getEach( page )
    }

    getEach( i ) {
        let data = this.props.getCityId;

        let start = 5 * (i - 1);
        let finish = (5 * i) > data.length ? data.length : 5 * i;
        let perPage = finish - start;

        while ( start < finish ) {
            let id = data[ start ];
            this.getSubreddit( id, perPage );
            start++;
        }
    }

    getSubreddit( id, total ) {
        let main = this;
        main.state.listWeather = [];
        fetch( 'http://api.openweathermap.org/data/2.5/weather?id=' + id + '&units=metric&appid=69465df65a432b95653f0f7d02072c98' )
        // fetch( 'http://api.openweathermap.org/data/2.5/weather?id=' + id +
        // '&units=metric&appid=120509f47800264392c68f94ff38fb25' )
            .then( response => {
                return response.json();
            } ).then( json => {
            main.state.listWeather.push( json );

            if ( main.state.listWeather.length === total ) {
                main.setState( {
                    load: true
                } )
            }
        } ).catch( err => {
            console.log( 'Cannot find subreddit.' );
        } );
    }

    getPages() {
        let total = this.props.getCityId.length;
        return total / 5;
    }

    componentDidUpdate( prevProps, prevState ) {
        if ( prevProps.getCityId !== this.props.getCityId ) {
            this.state.page = 1;
            this.state.statNext = this.getPages() >= 1;
            this.state.statPrev = false;
            this.getEach( this.state.page );
        }

        if ( this.state.listWeather.length > 0 ) {

            if ( !this.props.isfilter ) {
                return;
            }

            let main = this;

            let listW = this.state.listWeather;

            function compareAge( a, b ) {
                let forReturn = a.name < b.name;
                if ( main.props.sortAZ === false ) {
                    forReturn = a.name > b.name;
                }
                return forReturn;
            }

            listW.sort( compareAge );

            this.setState({
                listWeather: listW
            });

            this.props.set(IS_FILTER, false);
        }
    }

    componentDidMount() {
        this.getEach( this.state.page )
    }

    render() {
        return (
            <div className="home">
                <MySvg/>
                <Search/>
                {!this.state.load && <div className="spinner"></div>
                }
                <div className="home__cityList">
                    {this.state.load && <div>
                        {
                            this.state.listWeather.map( ( item, index ) => {
                                return <CartCity key={index} content={item}/>
                            } )
                        }
                    </div>
                    }
                </div>
                <div className="paging" onClick={this.onClickPagging}>
                    <NavButton val='prevew' isActive={this.state.statPrev}/>
                    <NavButton val='next' isActive={this.state.statNext}/>
                </div>
            </div>
        );
    }
}

Home.defaultProps = {
    sortAZ: null,
}

export default connect( state => ({
    getCityId: state.allCityID,
    sortAZ: state.sortAZ,
    isfilter: state.isfilter,
    getState: state
}), dispatch => ({
    set: ( action, content ) => {
        dispatch( {
            type: action,
            content: content
        } )
    },
}) )( Home );