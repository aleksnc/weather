import React, {Component} from 'react';
import {connect} from 'react-redux'

import './weather.scss';

class Weather extends Component {

    render() {
        const {
            name,
            main,
            wind,
        } = this.props.content


        return (
            <div className="weather">
                <span className="weather__CityName">
                    {name}
                </span>
                <span className="weather__val weather__val--c">
                    {Math.ceil(main.temp)}
                </span>

                <div className="weather__block">
                    <span className="weather__blockName">Real Feel: </span>
                    <span className="weather__data weather__data--temp-c">18</span>
                </div>
                <div className="weather__block">
                    <span className="weather__blockName">Wind: </span>
                    <span className="weather__data">{wind.speed} km/h</span>
                </div>
                <div className="weather__block">
                    <span className="weather__blockName">Pressure: </span>
                    <span className="weather__data"> {main.pressure} MB</span>
                </div>
                <div className="weather__block">
                    <span className="weather__blockName">Humidity: </span>
                    <span className="weather__data">{main.humidity}%</span>
                </div>
            </div>
        )

    }
}

export default connect(
    state => ({}),
    dispatch => ({})
)(Weather);