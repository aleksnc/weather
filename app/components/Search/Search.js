import React, {Component} from 'react';
import {connect} from 'react-redux'
import DATA from '../../utilits/city.list.min.json'
import {ALL_ID, SORT_AZ, IS_FILTER} from "../../constants";

import './search.scss'

class AboutWeather extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sort: true,
            textFilterName: 'A-Z'
        }

        this.onKeyUp = this.onKeyUp.bind(this);
        this.onSort = this.onSort.bind(this);
    }

    onKeyUp(e) {
        let text = e.target.value;
        this.props.set(ALL_ID,text);
        text =text.toUpperCase();

        function filterByName(obj) {
            if (obj.name.toUpperCase().indexOf(text)>-1) {
                return true;
            } else {
                return false;
            }
        }

        let searchArr = DATA.filter(filterByName);
        searchArr =searchArr.map((item, index)=>{
            return item.id;
        })

        this.props.set(ALL_ID,searchArr);
    }

        onSort(){
        this.props.set(SORT_AZ,this.state.sort);
        this.props.set(IS_FILTER,true);
        let sort = this.state.sort?false:true;
        let textFilterName = this.state.textFilterName=='A-Z'?'Z-A':'A-Z';
        this.setState({sort, textFilterName})
    }

    render() {

        return (
            <div className="search">
                <div className="search__search">
            <input type="text"
                   defaultValue=""
                   className="search__input"
                   placeholder="find city"
                   onKeyUp={this.onKeyUp}
            />
            </div>
                <div className="search__filter">
                    <button
                        className="search__filterBtn"
                        onClick={this.onSort}
                    >
                        {this.state.textFilterName}
                        </button>
                </div>
            </div>
        )

    }
}

export default connect(
    state => ({
        getText: state
    }),
    dispatch => ({
        set: (actions,content) => {
            dispatch({type: actions, content: content})
        },
    })
)(AboutWeather);