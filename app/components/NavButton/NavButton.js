import React, {Component} from 'react';
import './navButton.scss'

function NavButton({val, isActive}) {
    return (
        <div className="navButton">
            {
                val !== null && val !== undefined &&
                <button id={val} disabled={!isActive} className={"navButton__btn "+(isActive==false?'passive':'')}>
                    {val}
                </button>
            }
        </div>
    )
}

export default NavButton;