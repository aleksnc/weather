import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import WeatherLabel from '../WeatherLabel/WeatherLabel'
import './cartCity.scss'

class CartCity extends Component {
    constructor(props) {
        super(props);

        this.toggleActive = this.toggleActive.bind(this);
    }

    toggleActive(e) {
        let dataId = e.target.getAttribute('data-id')

        let like = [];
        let storageData = JSON.parse(localStorage.getItem("like"));
        if (storageData !== null) {
            like = storageData;
        }
        let arrID = [];
        arrID.push(parseInt(dataId));

        let newLike = like.filter(x => !arrID.includes(x));

        if (newLike.length === like.length) {
            newLike.push(parseInt(dataId));
        }

        this.props.set('LIKE_ID',newLike);

        localStorage.setItem("like", JSON.stringify(newLike));
    }

    isLiked() {
        let active = (this.props.getLikeId.indexOf(this.props.content.id) >= 0);

        return active ? 'active' : '';
    }

    render() {

        const {
            name,
            main,
            id,
            weather
        } = this.props.content

        return (
            <div className="cartCity__wrapper">
                <div className={"cartCity__star " + this.isLiked()} data-id={id} onClick={this.toggleActive}>
                    <svg viewBox="0 0 52 52">
                        <use className="ic-star" x="1" y="1" xlinkHref="#star"></use>
                    </svg>
                </div>
                <Link to={'/' + id} className="cartCity">
                    <span className="cartCity__name">{name}</span>
                    <div className="cartCity__img">
                        <WeatherLabel icon={weather}/>
                    </div>
                    <span className="cartCity__temp cartCity__temp--c">{Math.ceil(main.temp)}</span>
                </Link>

            </div>
        )

    }
}

CartCity.defaultProps = {
    content: null,
}

export default connect(
    state => ({
        getLikeId: state.likeCityID,
        getState: state,
    }),
    dispatch => ({
        set: (actions, content) => {
            dispatch({type: actions, content: content})
        },
    })
)(CartCity);