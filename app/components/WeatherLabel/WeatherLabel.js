import React from 'react';
import './weatherLabel.scss';

const WeatherLabel = ({icon}) => {
    let myClass = ((icon[0].icon !== null) || (icon[0].icon !== undefined)) ? "weatherLabel--" + icon[0].icon : '';
    return (
        <div className={"weatherLabel " + myClass}>
        </div>
    )
}
export default WeatherLabel;