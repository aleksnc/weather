import React, {Component} from 'react';
import {connect} from 'react-redux'
import Moment from 'react-moment';

import WeatherLabel from '../WeatherLabel/WeatherLabel'

import './sun.scss';

class Sun extends Component {

    render() {

        const {
            name,
            main,
            id,
            dt,
            weather,
            sys
        } = this.props.content

        var sunrise = new Date(sys.sunrise*1000);
        var sunset = new Date(sys.sunset*1000);

        return (
            <div className="sun">
             <div className="sun__type">
                 <WeatherLabel icon={weather}/>
             </div>
                <div className="sun__block">
                    <span className="sun__blockName">Sunrise: </span>
                    <p className="sun__data">
                        <Moment format="HH:mm">
                            {sunrise}
                        </Moment>
                    </p>
                </div>
                <div className="sun__block">
                    <span className="sun__blockName">Sunset: </span>
                    <p className="sun__data">
                        <Moment format="HH:mm">
                            {sunset}
                        </Moment>
                    </p>
                </div>
            </div>
        )

    }
}

export default connect(
    state => ({}),
    dispatch => ({})
)(Sun);