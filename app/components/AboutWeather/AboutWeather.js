import React, {Component} from 'react';
import {connect} from 'react-redux'
import Moment from 'react-moment';
import Weather from '../Weather/Weather'
import Sun from '../Sun/Sun'

import './aboutWeather.scss'

class AboutWeather extends Component {
    render() {

        const {
            dt,
        } = this.props.content

        let time = new Date(dt*1000);

        let Data = new Date(1518334200*1000+3600);

        return (
            <div className="aboutWeather">
                <div className="aboutWeather__date">
                    <p className="aboutWeather__month">
                        <Moment format="MMMM DD" >
                            {time}
                        </Moment>
                    </p>
                    <p className="aboutWeather__day">
                        <Moment format="dddd" >
                            {time}
                        </Moment>
                    </p>
                    <p className="aboutWeather__time">
                        <Moment format="HH:mm">
                            {time}
                        </Moment>
                    </p>
                </div>
                <div className="aboutWeather__weather">
                    <Weather content={this.props.content}/>
                    <Sun content={this.props.content}/>
                </div>
            </div>
        )

    }
}

export default AboutWeather;